// Bài tập 1
function sapXep() {
  var oSo1 = document.getElementById("txt-so-1").value * 1;
  var oSo2 = document.getElementById("txt-so-2").value * 1;
  var oSo3 = document.getElementById("txt-so-3").value * 1;
  console.log({ oSo1, oSo2, oSo3 });
  var result = null;
  if (oSo1 > oSo2) {
    if (oSo2 > oSo3) {
      result = `${oSo3} < ${oSo2} < ${oSo1}`;
    } else {
      if (oSo1 > oSo3) {
        result = `${oSo2} < ${oSo3} < ${oSo1}`;
      } else {
        result = `${oSo2} < ${oSo1} < ${oSo3}`;
      }
    }
  } else {
    if (oSo1 > oSo3) {
      result = `${oSo3} < ${oSo1} < ${oSo2}`;
    } else {
      if (oSo3 > oSo2) {
        result = `${oSo1} < ${oSo2} < ${oSo3}`;
      } else {
        result = `${oSo1} < ${oSo3} < ${oSo2}`;
      }
    }
  }

  document.getElementById("result").innerHTML = result;
}
// End bài tập 1

// Bài tập 2
function chaoHoi() {
  var thanhVien = document.getElementById("txt-thanh-vien").value;
  var result = null;
  switch (thanhVien) {
    case "B":
      document.getElementById("result-1").innerHTML = `Xin Chào Bố!`;
      break;
    case "M":
      document.getElementById("result-1").innerHTML = `Xin Chào Mẹ!`;
      break;

    case "A":
      document.getElementById("result-1").innerHTML = `Xin Chào Anh trai!`;
      break;

    case "E":
      document.getElementById("result-1").innerHTML = `Xin Chào Em gái!`;
      break;
    default:
      document.getElementById("result-1").innerHTML = `Xin Chào Người lạ!`;
  }
}

// End bài tập 2

// Bài tập 3
function demSo() {
  var oThu1 = document.getElementById("txt-o-1").value * 1;
  var oThu2 = document.getElementById("txt-o-2").value * 1;
  var oThu3 = document.getElementById("txt-o-3").value * 1;
  console.log(oThu1, oThu2, oThu3);
  var result = null;
  var soChan = (oThu1 % 2 == 0) + (oThu2 % 2 == 0) + (oThu3 % 2 == 0);
  var soLe = 3 - soChan;
  document.getElementById(
    "result-3"
  ).innerHTML = `Có ${soChan} Số chẵn,${soLe} Số lẻ`;
}

// End bài tập 3

// Bài tập 4
function duDoan() {
  var doDai1 = document.getElementById("txt-canh-1").value * 1;
  var doDai2 = document.getElementById("txt-canh-2").value * 1;
  var doDai3 = document.getElementById("txt-canh-3").value * 1;
  console.log(doDai1, doDai2, doDai3);
  var result = null;
  if (doDai1 == doDai2 && doDai1 == doDai3) {
    result = `Hình tam giác đều`;
  } else if (doDai1 == doDai2 || doDai1 == doDai3 || doDai2 == doDai3) {
    result = "Hình tam giác cân";
  } else {
    result = "Hình tam giác vuông";
  }

  document.getElementById("result-4").innerHTML = result;
}
// End bài tập 4
